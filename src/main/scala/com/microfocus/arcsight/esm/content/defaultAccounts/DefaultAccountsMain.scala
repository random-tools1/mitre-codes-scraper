package com.microfocus.arcsight.esm.content.defaultAccounts

import java.util.Date

import com.microfocus.arcsight.esm.content.Writter
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.{By, Keys, WebDriver, WebElement}
import scala.jdk.CollectionConverters._

object DefaultAccountsMain extends App {

  val OPEN_NEW_TAB_FOCUS = Keys.chord(Keys.CONTROL, Keys.SHIFT, Keys.ENTER)
  lazy val writer = new Writter("defaultAccounts.csv")

  processPage()

  def processPage(): Unit = {
    val init = System.currentTimeMillis()
    System.setProperty("webdriver.chrome.driver", "chromedriver_win32/chromedriver.exe");
    val browser: WebDriver = new ChromeDriver()
    try {
      browser.get("https://datarecovery.com/rd/default-passwords/")

      // process page
      println("processing")
      val tableRows = browser.findElements(By.xpath("//div[@class='table-responsive']/table/tbody/tr")).asScala
      val data = tableRows.map(processTable(browser))
      writeDefaultAccounts2CSV(data.flatten.toSeq)
    } catch {
      case e => {
        e.printStackTrace()
      }
    } finally {
      closeAllTabs(browser)
      browser.close()
      closeCSVWriter()
      val end = System.currentTimeMillis()
      println(s"Time processing: ${(end - init) / 60000.0}m")
      System.exit(0);
    }
  }

  def processTable(browser: WebDriver)(element: WebElement): Option[DefaultAccount] = {
    val cells = element.findElements(By.tagName("td")).asScala
    if (cells.isEmpty) {
      None
    } else {
      val texts = cells.map(_.getText)
      Some(DefaultAccount(texts(0), texts(1), texts(2), texts(3), texts(4), texts(5), texts(6), texts(7)))
    }
  }

  def closeAllTabs(browser: WebDriver): Unit = {
    browser.getWindowHandles.forEach( hdl => {
      browser.switchTo().window(hdl)
      browser.close()
    })
  }

  def closeCSVWriter(): Unit = {
    writer.close()
  }

  case class DefaultAccount private(val manufacturer: String, val model: String, val revision: String, val protocol: String,
                                    val user: String, val password: String, val accessLevel: String, val notes: String,
                                    timestamp: Date) extends Ordered[DefaultAccount] {
    override def compare(that: DefaultAccount): Int = {
      val manComp = manufacturer.compareTo(that.manufacturer)
      if (manComp == 0) {
        val modComp = model.compareTo(that.model)
        if (modComp == 0) {
          val revComp = revision.compareTo(that.revision)
          if (revComp == 0) {
            val userComp = user.compareTo(that.user)
            if (userComp == 0) {
              timestamp.compareTo(that.timestamp)
            } else userComp
          } else revComp
        } else modComp
      } else manComp
    }

    def toCSVLine: String = {
      List(manufacturer, model, revision, protocol, user, password, accessLevel, notes)
        .map(s => if (s.contains('"')) s""" "$s" """.trim else s )
        .mkString(",")
    }
  }

  def writeDefaultAccounts2CSV(accounts: Seq[DefaultAccount]): Unit = {
    writer.write(s"Manufacturer,Model,Revision,Protocol,User,Password,Access Level,Notes") // Header
    accounts.foreach(acc => {
      writer.write(s"${acc.toCSVLine}")
    })
  }

  object DefaultAccount {
    def apply(manufacturer: String, model: String, revision: String, protocol: String, user: String, password: String,
              accessLevel: String, notes: String): DefaultAccount = {
      DefaultAccount(manufacturer, model, revision, protocol, user, password, accessLevel, notes, new Date())
    }
  }
}
