package com.microfocus.arcsight.esm.content

import java.io.PrintWriter
import java.nio.charset.Charset

/**
 * Created by IntelliJ IDEA.
 * User: londo
 * Date: 15/02/12
 * Time: 11:58 AM
 */

class Writter(filePath: String) {
  lazy private val out: PrintWriter = new PrintWriter(filePath, Charset.forName("UTF-8"))

  def write(text: String) = {
    out.println(text)
    flush
  }

  def error(e:Exception) = write(e.getMessage)

  def close() = {
    flush()
    out.close()
  }
  def flush() = { out.flush() }
}
