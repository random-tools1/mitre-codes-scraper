package com.microfocus.arcsight.esm.content.mitre

import com.microfocus.arcsight.esm.content.Writter
import scala.jdk.CollectionConverters._
import org.openqa.selenium.{By, Keys, WebDriver, WebElement}
import org.openqa.selenium.chrome.ChromeDriver
import scala.collection.mutable.SortedSet

object MitreMain extends App {

  val OPEN_NEW_TAB_FOCUS = Keys.chord(Keys.CONTROL, Keys.SHIFT, Keys.ENTER)
  lazy val writer = new Writter("technics.csv")

  processMitrePage()

  def processMitrePage(): Unit = {
    val init = System.currentTimeMillis()
    System.setProperty("webdriver.chrome.driver", "chromedriver_win32/chromedriver.exe");
    val browser: WebDriver = new ChromeDriver()
    try {
      browser.get("https://attack.mitre.org/")

      // process page
      println("processing")
      val titleElements = browser.findElements(By.xpath("//a[@class='technique-mapping']")).asScala
      val data = titleElements.map(processTechnicPage(browser))
      val dataSet = data.foldLeft[SortedSet[(String, String, String)]](SortedSet.empty[(String, String, String)]){
        (accum, data) => accum ++ data
      }
      writeTechnic2CSV(dataSet)
    } catch {
      case e => {
        e.printStackTrace()
      }
    } finally {
      closeAllTabs(browser)
      browser.close()
      closeCSVWriter()
      val end = System.currentTimeMillis()
      println(s"Time processing: ${(end - init) / 60000.0}m")
      System.exit(0);
    }
  }

  def processTechnicPage(browser: WebDriver)(element: WebElement): SortedSet[(String, String, String)] = {
    val dataSet = SortedSet.empty[(String, String, String)]
    element.sendKeys(OPEN_NEW_TAB_FOCUS)
    val lastTab = browser.getWindowHandles.asScala.last
    browser.switchTo().window(lastTab)
    val technicName = browser.findElement(By.xpath("//h1")).getText
    var id = ""
    var tactics = ""

    val list = browser.findElements(By.xpath("//div[@class='card-data']"))
    list.forEach(str => {
      val pattern = "(\\w+)(:\\s)(.+)".r
      if (pattern.matches(str.getText)) {
        val pattern(a, b, c) = str.getText
        (a, c) match {
          case ("ID", _id) => id = _id
          case ("Tactic", _tactic) => tactics = _tactic
          case _ =>
        }
      }
    })
    // s"$id,$tactic,$name"
    tactics.split(",").foreach(tactic => dataSet.add((id.trim, tactic.trim, technicName.trim)) )
    browser.close()
    val firstTab = browser.getWindowHandles.asScala.head
    browser.switchTo().window(firstTab)
    dataSet
  }

  def closeAllTabs(browser: WebDriver): Unit = {
    browser.getWindowHandles.forEach( hdl => {
      browser.switchTo().window(hdl)
      browser.close()
    })
  }

  def closeCSVWriter(): Unit = {
    writer.close()
  }

  def writeTechnic2CSV(data: SortedSet[(String, String, String)]): Unit = {
    writer.write(s"ID,Tactic,Name") //Header
    data.foreach(d => {
      writer.write(s"${d._1},${d._2},${d._3}")
    })
  }

}
