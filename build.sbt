name := "mitre-codes-scraper"

version := "1.0"

scalaVersion := "2.13.1"

libraryDependencies += "org.seleniumhq.selenium" % "selenium-java" % "2.35.0"
libraryDependencies += "joda-time" % "joda-time" % "2.10.3"